*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI ${DSname}


*** Test Cases ***
TestGoogle
    [Documentation]   Checks accessibility to https://www.google.fr
    CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}
    Location Should Contain    ${Global.Url}
    
    Delete All Cookies
    Click Element       xpath://button[@id='W0wltc']//div[@role='none']
    #Capture Page Screenshot        filename=selenium-screenshot-{index}.png
    
    Click Element       xpath://textarea[@id='APjFqb']
    
    #Input Text      id:APjFqb       ${Google.DoResearch}
    Input Text      id:APjFqb       Linux
    #Capture Page Screenshot        filename=selenium-screenshot-{index}.png
    
    Click Element       xpath://div[@id='jZ2SBf']//div[1]
    #Capture Page Screenshot        filename=selenium-screenshot-{index}.png
    
    Click Link      xpath://a[@href='https://www.linux.org/'][1]
    #links are searched using id, name, href and the link text.
    #FindByXpath("//body/div[@id='main']/div[@id='cnt']/div[@id='rcnt']/div[@id='center_col']/div[@id='res']/div[@id='search']/div[@data-hveid='CAIQBA']/div[@id='rso']/div/div/div[@jsname='TlEBqd']/div[@id='_B0VKZP-0NuatkdUP9teVqAY_29']/div[@id='_B0VKZP-0NuatkdUP9teVqAY_30']/div/div[@id='kp-wp-tab-cont-overview']/div/div/div/div[@id='kp-wp-tab-overview']/div[1]")
    #Click Link      xpath://a[@href=div[352]
    #Click Link      xpath://cite[@role='text'])[15]
    #content > div > cr-realbox-match:nth-child(3)
    #//*[@id="content"]/div/cr-realbox-match[2]
    Capture Page Screenshot         filename=selenium-screenshot-{index}.png

