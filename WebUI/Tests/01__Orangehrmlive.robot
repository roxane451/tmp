*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI ${DSname}

*** Test Cases ***
TestOrange
    [Documentation]   Checks accessibility to https://opensource-demo.orangehrmlive.com/
    #In SeleniumLibrary, browser is a synonym for WebDriver instance.
    CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}
    #Windows are children of a browser
    Maximize Browser Window
    Click Element    /html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/form[1]/div[1]/div[1]/div[2]/input[1] 
    Input Text       name:Username      Admin
    Click button    xpath://button[@type='submit']
    Close Browser
#/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/form[1]/div[1]/div[1]/div[2]/input[1]
#//button[@type='submit']
#input[placeholder='Username']
#"Username"
#
