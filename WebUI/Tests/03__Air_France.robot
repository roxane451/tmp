*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI ${DSname}
Suite Teardown     Close All Browsers

*** Test Cases ***
TestAirfrance
    [Documentation]   Checks accessibility to https://wwws.airfrance.fr/    
    #CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}  
    #CCT Open Browser    ${Global.Url}       ${Global.Browser}    headlesschrome
    
    CCT Open Browser    ${Global.Url}   ${Global.Browser}     headlesschrome
    Click Element   xpath://*[@id="//button[normalize-space()='Refuser']"]
    Click Element   xpath://span[normalize-space()='Continuer']
    Input Text      css:#mat-input-1  Lyon
    Click Element   xpath://div[@class='bw-search-station-item']
    Input Text      xpath://input[@id='mat-input-2']  Nice
    Click Element   xpath://div[@class='bw-search-station-item__station-data']
    Click Element   xpath://span[normalize-space()='Rechercher les vols']
    Capture Page Screenshot
    [Teardown]    Close All Browsers

