*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI ${DSname}


*** Test Cases ***
TestHeroku
    [Documentation]   Checks accessibility to https://testpages.herokuapp.com/styled/validation/input-validation.html
    CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}
    Location Should Contain    ${Global.Url}
    Click Element      xpath://input[@id='firstname']
    Input Text      id:firstname    Admin
    Click Element      xpath://input[@id='age']
    Input Text      id:age    18
    Click Element    xpath://input[@type='submit']
    Capture Page Screenshot
    Close Browser
